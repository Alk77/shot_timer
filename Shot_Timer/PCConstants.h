//
//  PCConstants.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 21.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCConstants : NSObject

#pragma mark - user defaults keys

extern NSString * const shotResultsKey;
extern NSString * const delayTimeKey;
extern NSString * const sensitivityKey;
extern NSString * const isRandomTimeKey;

@end
