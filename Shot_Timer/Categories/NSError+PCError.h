//
//  NSError+PCError.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 01.02.2018.
//  Copyright © 2018 ProCreationsMac. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
	
	PCCannotLaunchOnSimulaton = 1000,
	PCWrongAudioEngineInit = 1001	
	
} CSMErrorCode;

@interface NSError (CSMError)

+ (NSString * _Nonnull)getErrorMessageWithCode:(NSUInteger)errorCode;
+ (NSError * _Nonnull)getErrorWithErrorCode:(NSUInteger)errorCode;


@end
