//
//  NSError+PCError.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 01.02.2018.
//  Copyright © 2018 ProCreationsMac. All rights reserved.
//

#import "NSError+PCError.h"

@implementation NSError (PCError)

+ (NSString * _Nonnull)getErrorMessageWithCode:(NSUInteger)errorCode
{
	NSString *errorMessage = @"Error";
	
	switch (errorCode) {
		case PCCannotLaunchOnSimulaton:
			errorMessage = @"Sorry, you cannot launch Audio Engine on the simulator.";
			break;
			
		case PCWrongAudioEngineInit:
			errorMessage = @"Something went wrong. Unable to initialize second instance of Audio Engine.";
			break;
			
		default:
			errorMessage = @"Unknown error.";
			break;
	}
	
	return errorMessage;
}

+ (NSError * _Nonnull)getErrorWithErrorCode:(NSUInteger)errorCode
{
	return [NSError errorWithDomain:@"PCShotTimerDomain" code:errorCode userInfo:@{NSLocalizedDescriptionKey : [NSError getErrorMessageWithCode:errorCode]}];
}

@end
