//
//  PCAlertHelper.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 21.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import "PCAlertHelper.h"

@implementation PCAlertHelper
{
	BOOL isShowAlertView;
}

+ (PCAlertHelper *)sharedHelper {
	static dispatch_once_t onceToken;
	static PCAlertHelper *sharedHelper = nil;	
	dispatch_once(&onceToken, ^{
		sharedHelper = [[PCAlertHelper alloc] init];
	});
	
	return sharedHelper;
}

- (UIAlertController*)showAlertViewWithTitle:(NSString*)title message:(NSString*)message {
	if (isShowAlertView)
		return nil;
	isShowAlertView = YES;
	
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
		isShowAlertView = NO;
		}];
	[alertController addAction:cancelAction];
	return alertController;
}

@end
