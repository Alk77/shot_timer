//
//  PCAlertHelper.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 21.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PCAlertHelper : NSObject

+ (PCAlertHelper *)sharedHelper;

- (UIAlertController*)showAlertViewWithTitle:(NSString*)title message:(NSString*)message;

@end
