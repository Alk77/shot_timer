//
//  PCResultsPresenter.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 26.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PCShootingResults;
@protocol IPCResultsProtocol;

@interface PCResultsPresenter : NSObject

@property (nonatomic, strong) NSMutableArray<PCShootingResults*> *results;

//- (NSMutableArray*)results;
- (void)updateResultsInView:(id<IPCResultsProtocol>)view;
- (void)deleteResult:(NSUInteger)item inView:(id<IPCResultsProtocol>)view;

@end
