//
//  PCResultsViewController.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 26.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import "PCResultsViewController.h"
#import "PCResultsPresenter.h"
#import "PCResultsTableViewCell.h"
#import "PCShootingResults.h"

@interface PCResultsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) PCResultsPresenter *presenter;
@property (weak, nonatomic) IBOutlet UITableView *resultsTableView;

@end

static NSString * const tableViewCellName = @"PCResultsTableViewCell";

@implementation PCResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];	
	self.presenter = [[PCResultsPresenter alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.presenter updateResultsInView:self];
}

#pragma mark - view protocol methods

- (void)updateResultsTableView {
	[self.resultsTableView reloadData];
}

#pragma mark - table view delegates

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.presenter.results count];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
	PCResultsTableViewCell *cell = (PCResultsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:tableViewCellName];
	PCShootingResults *result = self.presenter.results[indexPath.row];
	NSString *dateString = [NSDateFormatter localizedStringFromDate:result.shootingDate
														  dateStyle:NSDateFormatterMediumStyle
														  timeStyle:NSDateFormatterMediumStyle];
	cell.dateLabel.text = dateString;
	cell.overallTimeResultsLabel.text = [NSString stringWithFormat:@"%.2f", [result.overallTime floatValue]];
	cell.firstShotTimeResultsLabel.text = [NSString stringWithFormat:@"%.2f", [result.firstShotTime floatValue]];
	cell.shotsAmountResultsLabel.text = [NSString stringWithFormat:@"%d", [result.shotsAmount intValue]];
	cell.timePerShotResultsLabel.text = [NSString stringWithFormat:@"%.2f", [result.timePerShot floatValue]];
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 80;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		[self.presenter deleteResult:indexPath.row inView:self];
		[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
	}
}

@end
