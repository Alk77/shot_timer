//
//  PCResultsPresenter.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 26.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import "PCResultsPresenter.h"
#import "IPCResultsProtocol.h"
#import "PCConstants.h"
#import "PCShootingResults.h"

@implementation PCResultsPresenter

- (instancetype)init {
	self = [super init];
	if (self) {
		NSArray *resultsArray = [[NSUserDefaults standardUserDefaults] objectForKey:shotResultsKey];
		self.results = [[NSMutableArray alloc] init];
		for (NSData *data in resultsArray) {
			PCShootingResults *result = [NSKeyedUnarchiver unarchiveObjectWithData:data];
			if (result)
				[self.results addObject:result];
		}
	}
	return self;
}

- (void)updateResultsInView:(id<IPCResultsProtocol>)view {
	[self.results removeAllObjects];
	NSArray *resultsArray = [[NSUserDefaults standardUserDefaults] objectForKey:shotResultsKey];
	for (NSData *data in resultsArray) {
		PCShootingResults *result = [NSKeyedUnarchiver unarchiveObjectWithData:data];
		if (result)
			[self.results addObject:result];
	}
	[view updateResultsTableView];
}

- (void)deleteResult:(NSUInteger)item inView:(id<IPCResultsProtocol>)view {
	if ([self.results count] > item)
		[self.results removeObjectAtIndex:item];
	[view updateResultsTableView];
}

@end
