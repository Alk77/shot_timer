//
//  PCResultsViewController.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 26.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IPCResultsProtocol.h"

@interface PCResultsViewController : UIViewController <IPCResultsProtocol>

@end
