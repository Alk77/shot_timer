//
//  IPCResultsProtocol.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 01.02.2018.
//  Copyright © 2018 ProCreationsMac. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IPCResultsProtocol <NSObject>

- (void)updateResultsTableView;

@end
