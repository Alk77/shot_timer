//
//  PCFAQViewController.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 18.01.2018.
//  Copyright © 2018 ProCreationsMac. All rights reserved.
//

#import "PCFAQViewController.h"

@interface PCFAQViewController ()

@property (weak, nonatomic) IBOutlet UITextView *faqTextView;

@end

@implementation PCFAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		NSString * language = [[NSLocale preferredLanguages] firstObject];
		NSURL *textURL;
		if ([[language substringToIndex:2] isEqualToString:@"ru"])
		{
			textURL = [[NSBundle mainBundle] URLForResource: @"faq_ru" withExtension: @"rtf"];
		}
		else
		{
			textURL = [[NSBundle mainBundle] URLForResource: @"faq_en" withExtension: @"rtf"];
		}
		NSDictionary *options =
		@{NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType};
		
		NSAttributedString *text = [[NSAttributedString alloc] initWithURL: textURL
																   options: options
														documentAttributes: nil
																	 error: nil];
		dispatch_async(dispatch_get_main_queue(), ^{
			self.faqTextView.attributedText = text;
			self.faqTextView.textColor = [UIColor colorWithWhite:235.0 / 255.0 alpha:1.0];
		});
	});
}

@end
