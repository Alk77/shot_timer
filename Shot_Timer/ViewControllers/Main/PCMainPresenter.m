//
//  PCMainPresenter.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 20.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import "PCMainPresenter.h"
#import "IPCMainViewProtocol.h"
#import "NSError+PCError.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <Accelerate/Accelerate.h>
#import "PCShootingResults.h"
#import "PCConstants.h"

@interface PCMainPresenter () <AVAudioRecorderDelegate>

@end

@implementation PCMainPresenter
{
	NSMutableDictionary *shots;
	PCShootingResults *shotResults;
	AVAudioEngine *engine;
	AVAudioUnitEQ *EQUnit;
	NSUInteger shotsAmount;
	double previousPeak;
	float sensitivity;
}

- (instancetype)init {
	self = [super init];
	if (self)
	{		
		[[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryMultiRoute error:nil];
	}
	return self;
}

- (float)delayTime {
	if ([[NSUserDefaults standardUserDefaults] boolForKey:isRandomTimeKey])
		return -1.0;
	else
		NSLog(@"delay time: %f", [[NSUserDefaults standardUserDefaults] floatForKey:delayTimeKey]);
		return [[NSUserDefaults standardUserDefaults] floatForKey:delayTimeKey];
}

- (void)startTimerInView:(id<IPCMainViewProtocol>)view {
	shotResults = nil;
	NSError *error = nil;
#if TARGET_OS_SIMULATOR
	[view startTimerUpdateViewWithError:[NSError getErrorWithErrorCode:PCCannotLaunchOnSimulaton]];
	return;
#endif
	if (!engine)
	{
		engine = [[AVAudioEngine alloc] init];
		EQUnit = [[AVAudioUnitEQ alloc] initWithNumberOfBands:2];
		EQUnit.globalGain = 1.0;
		[engine attachNode:EQUnit];
		
		AVAudioUnitEQFilterParameters *filterParams = EQUnit.bands[0];
		filterParams.filterType = AVAudioUnitEQFilterTypeBandPass;
		filterParams.frequency = 200.0;
		filterParams.bandwidth = 5.0;
		filterParams.bypass = false;
		filterParams.gain = 24.0;
		[engine connect:engine.inputNode to:EQUnit format:[EQUnit outputFormatForBus:0]];
		[engine connect:EQUnit to:engine.mainMixerNode format:[engine.mainMixerNode outputFormatForBus:0]];
		
		previousPeak = 1.0;
		shotsAmount = 0;
		shots = [[NSMutableDictionary alloc] init];
		[shots setObject:[NSDate date] forKey:@"start"];
		
		[engine.mainMixerNode installTapOnBus:0 bufferSize:1024 format:[engine.mainMixerNode outputFormatForBus:0] block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
			[buffer setFrameLength:1024];
			UInt32 inNumberFrames = buffer.frameLength;
			
			if(buffer.format.channelCount>0)
			{
				Float32* samples = (Float32*)buffer.floatChannelData[0];
				Float32 maxValue = 0;
				
				vDSP_maxmgv((Float32*)samples, 1, &maxValue, inNumberFrames);
				NSLog(@"new value: %f", maxValue);
				if (previousPeak * 12.0 < maxValue && maxValue > [[NSUserDefaults standardUserDefaults] floatForKey:sensitivityKey])
				{
					++shotsAmount;
					[shots setObject:[NSDate date] forKey:[NSNumber numberWithUnsignedInteger:shotsAmount]];
				}
				previousPeak = maxValue;
			}
		}];
		if (![engine startAndReturnError:&error])
			[view startTimerUpdateViewWithError:error];
		else
			[view startTimerUpdateViewWithError:nil];
	}
	else
	{
		[view startTimerUpdateViewWithError:[NSError getErrorWithErrorCode:PCWrongAudioEngineInit]];
		engine = nil;
		EQUnit = nil;
	}
}

- (void)stopTimerInView:(id<IPCMainViewProtocol>)view {
	[shots setObject:[NSDate date] forKey:@"stop"];
	engine = nil;
	EQUnit = nil;
	[self getResultsObjectFromDictionary];
	[view stopTimerUpdateViewWithOverallTime:[shotResults.overallTime floatValue] firstShotTime:[shotResults.firstShotTime floatValue] shotsAmount:[shotResults.shotsAmount intValue] timePerShot:[shotResults.timePerShot floatValue]];
}

- (void)saveResultsInView:(id<IPCMainViewProtocol>)view {
	NSMutableArray *results = [[[NSUserDefaults standardUserDefaults] objectForKey:shotResultsKey] mutableCopy];
	if (!results) {
		results = [[NSMutableArray alloc] init];
	}
	NSData *data = [NSKeyedArchiver archivedDataWithRootObject:shotResults];
	[results addObject:data];
	[[NSUserDefaults standardUserDefaults] setObject:results forKey:shotResultsKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
	[view saveResultsUpdateView];
}

#pragma mark - service methods

- (void)getResultsObjectFromDictionary {
	int shotsAmount = ([shots count] > 1 ? (int)[shots count] - 2 : 0);
	NSTimeInterval overallTime = [[shots objectForKey:@"stop"] timeIntervalSinceDate:[shots objectForKey:@"start"]];
	NSTimeInterval firstShotTime = 0.0;
	NSTimeInterval timePerShot = 0.0;
	if (shotsAmount > 0)
	{
		firstShotTime = [[shots objectForKey:@1] timeIntervalSinceDate:[shots objectForKey:@"start"]];
		NSTimeInterval shotsTime = [[shots objectForKey:[NSNumber numberWithUnsignedInteger:[shots count] - 2]] timeIntervalSinceDate:[shots objectForKey:@"start"]];
		timePerShot = shotsTime / shotsAmount;
	}
	shotResults = [[PCShootingResults alloc] initWithOverallTime:overallTime firstShotTime:firstShotTime shotsAmount:shotsAmount timePerShot:timePerShot];
}

@end
