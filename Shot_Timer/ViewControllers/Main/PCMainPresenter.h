//
//  PCMainPresenter.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 20.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PCShotResults;
@protocol IPCMainViewProtocol;

@interface PCMainPresenter : NSObject

- (float)delayTime;
- (void)startTimerInView:(id<IPCMainViewProtocol>)view;
- (void)stopTimerInView:(id<IPCMainViewProtocol>)view;
- (void)saveResultsInView:(id<IPCMainViewProtocol>)view;

@end
