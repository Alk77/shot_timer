//
//  PCMainViewController.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 20.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import "PCMainViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "PCMainPresenter.h"
#import "PCConstants.h"
#import "PCSliderView.h"
#import "PCAlertHelper.h"

@interface PCMainViewController () <UITabBarControllerDelegate, AVAudioPlayerDelegate>

@property (nonatomic, strong) PCMainPresenter *presenter;
@property (weak, nonatomic) IBOutlet PCSliderView *slider;
@property (weak, nonatomic) IBOutlet UILabel *overallTimeCounter;
@property (weak, nonatomic) IBOutlet UILabel *firstShotTimeCounter;
@property (weak, nonatomic) IBOutlet UILabel *shotsAmountCounter;
@property (weak, nonatomic) IBOutlet UILabel *timePerShotCounter;

@end

@implementation PCMainViewController
{
	AVAudioPlayer *player;
	BOOL disabledTabBarController;	
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.presenter = [[PCMainPresenter alloc] init];
	self.slider.parentVC = self;
	
	self.tabBarController.delegate = self;
	
	NSString *pathToSound = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
	NSURL *audioURL = [NSURL fileURLWithPath:pathToSound];
	player = [[AVAudioPlayer alloc] initWithContentsOfURL:audioURL fileTypeHint:@"mp3" error:nil];
	player.delegate = self;
	[player setVolume:1.0];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	self.slider.delayForStart = [self.presenter delayTime];
}

#pragma mark - protocol methods

- (void)startTimerAction {
	[player play];
	[self.presenter startTimerInView:self];
}

- (void)startTimerUpdateViewWithError:(NSError*)error {
	disabledTabBarController = YES;
	if (error) {
		UIAlertController *alert = [[PCAlertHelper sharedHelper] showAlertViewWithTitle:@"Error" message:error.localizedDescription];
		[self.tabBarController presentViewController:alert animated:YES completion:nil];
	}
}

- (void)stopTimerAction {
	[self.presenter stopTimerInView:self];
}

- (void)stopTimerUpdateViewWithOverallTime:(float)overallTime firstShotTime:(float)firstShotTime shotsAmount:(int)shotsAmount timePerShot:(float)timePerShot {
	self.overallTimeCounter.text = [NSString stringWithFormat:@"%.2f", overallTime];
	self.firstShotTimeCounter.text = [NSString stringWithFormat:@"%.2f", firstShotTime];
	self.shotsAmountCounter.text = [NSString stringWithFormat:@"%d", (int)shotsAmount];
	self.timePerShotCounter.text = [NSString stringWithFormat:@"%.2f", timePerShot];
}

- (void)saveResultsAction {
	[self.presenter saveResultsInView:self];	
}

- (void)saveResultsUpdateView {
	self.overallTimeCounter.text = @"0.0";
	self.firstShotTimeCounter.text = @"0.0";
	self.shotsAmountCounter.text = @"0";
	self.timePerShotCounter.text = @"0.0";
	disabledTabBarController = NO;
}

- (void)deleteResultsAction {
	self.overallTimeCounter.text = @"0.0";
	self.firstShotTimeCounter.text = @"0.0";
	self.shotsAmountCounter.text = @"0";
	self.timePerShotCounter.text = @"0.0";
	disabledTabBarController = NO;
}

#pragma mark - tab bar controller delegates

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
	if (disabledTabBarController)
		return NO;
	return YES;
}

@end
