//
//  IPCMainViewProtocol.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 10.01.2018.
//  Copyright © 2018 ProCreationsMac. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IPCMainViewProtocol <NSObject>

@required

- (void)startTimerAction;
- (void)startTimerUpdateViewWithError:(NSError*)error;

- (void)stopTimerAction;
- (void)stopTimerUpdateViewWithOverallTime:(float)overallTime firstShotTime:(float)firstShotTime shotsAmount:(int)shotsAmount timePerShot:(float)timePerShot;

- (void)saveResultsAction;
- (void)saveResultsUpdateView;

- (void)deleteResultsAction;

@end
