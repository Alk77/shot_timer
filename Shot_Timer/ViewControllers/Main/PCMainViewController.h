//
//  PCMainViewController.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 20.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IPCMainViewProtocol.h"

@interface PCMainViewController : UIViewController <IPCMainViewProtocol>

@end
