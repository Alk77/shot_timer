//
//  PCSettingsPresenter.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 26.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import "PCSettingsPresenter.h"
#import "PCConstants.h"

@implementation PCSettingsPresenter

- (void)changeSensitivity:(float)value {
	[[NSUserDefaults standardUserDefaults] setFloat:value forKey:sensitivityKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (float)getSensitivity {
	return [[NSUserDefaults standardUserDefaults] floatForKey:sensitivityKey];
}

- (void)changeDelayTime:(float)value {	
	[[NSUserDefaults standardUserDefaults] setFloat:value forKey:delayTimeKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
	NSLog(@"set delay time: %f", [[NSUserDefaults standardUserDefaults] floatForKey:delayTimeKey]);
}

- (float)getDelayTime {
	return [[NSUserDefaults standardUserDefaults] floatForKey:delayTimeKey];
}

- (void)defineRandomTime:(BOOL)value {
	[[NSUserDefaults standardUserDefaults] setBool:value forKey:isRandomTimeKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isRandomTime {
	return [[NSUserDefaults standardUserDefaults] boolForKey:isRandomTimeKey];
}

@end
