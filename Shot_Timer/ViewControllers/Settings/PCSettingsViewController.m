//
//  PCSettingsViewController.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 20.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import "PCSettingsViewController.h"
#import "PCSettingsPresenter.h"

@interface PCSettingsViewController ()

@property (nonatomic, strong) PCSettingsPresenter *presenter;
@property (weak, nonatomic) IBOutlet UILabel *sensitivityLabel;
@property (weak, nonatomic) IBOutlet UISlider *sensitivitySlider;
@property (weak, nonatomic) IBOutlet UILabel *delayTimeLabel;
@property (weak, nonatomic) IBOutlet UISlider *delayTimeSlider;
@property (weak, nonatomic) IBOutlet UIButton *randomDelayButton;

@end

@implementation PCSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.presenter = [[PCSettingsPresenter alloc] init];
	
	[self.sensitivitySlider setThumbImage:[UIImage imageNamed:@"slider"] forState:UIControlStateNormal];
	[self.sensitivitySlider setValue:[self.presenter getSensitivity]];
	[self.delayTimeSlider setThumbImage:[UIImage imageNamed:@"slider"] forState:UIControlStateNormal];
	[self.delayTimeSlider setValue:[self.presenter getDelayTime]];
	[self.randomDelayButton setImage:[UIImage imageNamed:@"switchOff"] forState:UIControlStateNormal];
	[self.randomDelayButton setImage:[UIImage imageNamed:@"switchOn"] forState:UIControlStateSelected];
	self.sensitivityLabel.text = [NSString stringWithFormat:@"%@: %.0f%%", NSLocalizedString(@"Sensitivity", @"Sensitivity"), self.sensitivitySlider.value];
	if ([self.presenter isRandomTime])
	{
		self.delayTimeLabel.text = NSLocalizedString(@"Delay Time", @"Delay Time");
		[self.delayTimeSlider setEnabled:NO];
	}
	else
	{
		self.delayTimeLabel.text = [NSString stringWithFormat:@"%@: %.1f%@", NSLocalizedString(@"Delay Time", @"Delay Time"), self.delayTimeSlider.value, NSLocalizedString(@"s", @"seconds")];
		[self.delayTimeSlider setEnabled:YES];
	}
	[self.randomDelayButton setSelected:[self.presenter isRandomTime]];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	[self.presenter changeSensitivity:self.sensitivitySlider.value];
	[self.presenter changeDelayTime:self.delayTimeSlider.value];
	[self.presenter defineRandomTime:self.randomDelayButton.selected];
}

#pragma mark - button handlers

- (IBAction)sensitivityChanged:(UISlider*)sender {
	self.sensitivityLabel.text = [NSString stringWithFormat:@"%@: %.0f%%", NSLocalizedString(@"Sensitivity", @"Sensitivity"), self.sensitivitySlider.value];
}

- (IBAction)delayTimeChanged:(UISlider *)sender {
	float normalizedValue = ((NSUInteger)(2 * sender.value + 0.5)) / 2.0;
	[sender setValue:normalizedValue animated:NO];
	self.delayTimeLabel.text = [NSString stringWithFormat:@"%@: %.1f%@", NSLocalizedString(@"Delay Time", @"Delay Time"), self.delayTimeSlider.value, NSLocalizedString(@"s", @"seconds")];
}

- (IBAction)randomValueChanged:(UIButton *)sender {
	[self.randomDelayButton setSelected:!self.randomDelayButton.selected];
	if (self.randomDelayButton.selected)
	{
		self.delayTimeLabel.text = NSLocalizedString(@"Delay Time", @"Delay Time");
		[self.delayTimeSlider setEnabled:NO];
	}
	else
	{
		self.delayTimeLabel.text = [NSString stringWithFormat:@"%@: %.1f%@", NSLocalizedString(@"Delay Time", @"Delay Time"), self.delayTimeSlider.value, NSLocalizedString(@"s", @"seconds")];
		[self.delayTimeSlider setEnabled:YES];
	}
}

@end
