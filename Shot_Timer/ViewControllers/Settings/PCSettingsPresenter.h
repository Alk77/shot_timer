//
//  PCSettingsPresenter.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 26.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCSettingsPresenter : NSObject

- (void)changeSensitivity:(float)value;
- (float)getSensitivity;
- (void)changeDelayTime:(float)value;
- (float)getDelayTime;
- (void)defineRandomTime:(BOOL)value;
- (BOOL)isRandomTime;


@end
