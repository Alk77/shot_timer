//
//  PCResultsTableViewCell.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 10.01.2018.
//  Copyright © 2018 ProCreationsMac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCResultsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *overallTimeResultsLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstShotTimeResultsLabel;
@property (weak, nonatomic) IBOutlet UILabel *shotsAmountResultsLabel;
@property (weak, nonatomic) IBOutlet UILabel *timePerShotResultsLabel;

@end
