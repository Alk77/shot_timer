//
//  PCSliderView.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 20.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import "PCSliderView.h"
#import "PCConstants.h"
#import "IPCMainViewProtocol.h"

@implementation PCSliderView 
{
	UIButton *startButton;
	CAGradientLayer *startButtonGradient;
	
	UIView *stopButton;
	UIImageView *rowImage;
	UILabel *swipeTextLabel;
	
	UIButton *saveButton;
	UIButton *deleteButton;
}

- (void)drawRect:(CGRect)rect {
	self.layer.backgroundColor = [UIColor blackColor].CGColor;
	self.layer.cornerRadius = 5.0;
	self.layer.borderColor = [UIColor grayColor].CGColor;
	self.layer.borderWidth = 2.0;
	
	startButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.origin.x + 2, self.frame.origin.y + 2, self.frame.size.width - 4, self.frame.size.height - 4)];
	startButton.layer.cornerRadius = 3.0;
	startButtonGradient = [CAGradientLayer layer];
	startButtonGradient.frame = startButton.bounds;
	startButtonGradient.colors = @[(id)[UIColor colorWithRed:(240.0/255.0) green:(55.0/255.0) blue:(55.0/255.0) alpha:1.0].CGColor, (id)[UIColor colorWithRed:(140.0/255.0) green:(24.0/255.0) blue:(24.0/255.0) alpha:1.0].CGColor];
	[startButton.layer insertSublayer:startButtonGradient atIndex:0];
	[startButton setTitle:NSLocalizedString(@"Start", @"Start") forState:UIControlStateNormal];
	[startButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
	[startButton addTarget:self action:@selector(buttonStartAction:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:startButton];
}

#pragma mark - button actions

- (void)buttonStartAction:(UIButton*)sender {
	[startButton removeTarget:self action:@selector(buttonStartAction:) forControlEvents:UIControlEventTouchUpInside];
	startButtonGradient.colors = @[(id)[UIColor colorWithRed:(255.0/255.0) green:(217.0/255.0) blue:(2.0/255.0) alpha:1.0].CGColor, (id)[UIColor colorWithRed:(246.0/255.0) green:(165.0/255.0) blue:(22.0/255.0) alpha:1.0].CGColor];
	[startButton setTitle:NSLocalizedString(@"Wait", @"Wait") forState:UIControlStateNormal];
	[startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
	float delay;
	if (self.delayForStart == -1.0)
		delay = (float)(arc4random() % 8) / 2.0;
	else
		delay = self.delayForStart;
	[self performSelector:@selector(startTimer) withObject:nil afterDelay:delay];
}

- (void)swipeAction:(UISwipeGestureRecognizer*)sender {
	[stopButton removeGestureRecognizer:sender];
	[UIView animateWithDuration:0.15
						  delay:0.0
						options:0
					 animations:^ {
						 stopButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, self.frame.size.width - stopButton.frame.size.width - 4, 0);
					 }
					 completion:^(BOOL finished) {
						 [self.parentVC stopTimerAction];
						 
						 [self performSelectorOnMainThread:@selector(removeStopButton) withObject:nil waitUntilDone:NO];
					 }];
	
}

- (void)buttonSaveAction:(UIButton*)sender {
	[self.parentVC saveResultsAction];
	[self removeSaveDeleteButtons];
}

- (void)buttonDeleteAction:(UIButton*)sender {
	[self.parentVC deleteResultsAction];
	[self removeSaveDeleteButtons];
}

#pragma mark - service methods

- (void)startTimer {
	[self.parentVC startTimerAction];

	[startButton removeFromSuperview];
	startButton = nil;
	
	rowImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.origin.x + self.frame.size.width * 0.7, self.frame.origin.y + self.frame.size.height * 0.3, self.frame.size.width * 0.25, self.frame.size.height * 0.4)];
	rowImage.image = [UIImage imageNamed:@"row"];
	rowImage.contentMode = UIViewContentModeScaleAspectFit;
	[self addSubview:rowImage];
	
	swipeTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.origin.x + self.frame.size.width * 0.3, self.frame.origin.y + 2, self.frame.size.width * 0.4, self.frame.size.height - 4)];
	swipeTextLabel.text = NSLocalizedString(@"Swipe to stop", @"Swipe to stop");
	swipeTextLabel.textAlignment = NSTextAlignmentCenter;
	swipeTextLabel.adjustsFontSizeToFitWidth = YES;
	swipeTextLabel.textColor = [UIColor grayColor];
	swipeTextLabel.numberOfLines = 2;
	[self addSubview:swipeTextLabel];
	
	stopButton = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x + 2, self.frame.origin.y + 2, self.frame.size.width * 0.25, self.frame.size.height - 4)];
	stopButton.layer.cornerRadius = 3.0;
	CAGradientLayer *stopButtonGradient = [CAGradientLayer layer];
	stopButtonGradient.frame = stopButton.bounds;
	stopButtonGradient.colors = @[(id)[UIColor colorWithRed:(240.0/255.0) green:(55.0/255.0) blue:(55.0/255.0) alpha:1.0].CGColor, (id)[UIColor colorWithRed:(140.0/255.0) green:(24.0/255.0) blue:(24.0/255.0) alpha:1.0].CGColor];
	[stopButton.layer insertSublayer:stopButtonGradient atIndex:0];
	UILabel *stopLabel = [[UILabel alloc] initWithFrame:CGRectMake(stopButton.frame.origin.x, stopButton.frame.origin.y + stopButton.frame.size.height * 0.25, stopButton.frame.size.width, stopButton.frame.size.height * 0.4)];
	stopLabel.text = NSLocalizedString(@"Stop", @"Stop");
	stopLabel.textColor = [UIColor whiteColor];
	stopLabel.textAlignment = NSTextAlignmentCenter;
	[stopButton addSubview:stopLabel];
	[self addSubview:stopButton];
	
	UISwipeGestureRecognizer *gr = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction:)];
	gr.direction = UISwipeGestureRecognizerDirectionRight;
	gr.delegate = self;
	[stopButton addGestureRecognizer:gr];
}

- (void)removeStopButton {
	[stopButton removeFromSuperview];
	stopButton = nil;
	[rowImage removeFromSuperview];
	rowImage = nil;
	[swipeTextLabel removeFromSuperview];
	swipeTextLabel = nil;
	
	saveButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.origin.x + 2, self.frame.origin.y + 2, self.frame.size.width * 0.5 - 2, self.frame.size.height - 4)];
	saveButton.layer.cornerRadius = 3.0;
	CAGradientLayer *saveButtonGradient = [CAGradientLayer layer];
	saveButtonGradient.frame = saveButton.bounds;
	saveButtonGradient.colors = @[(id)[UIColor colorWithRed:(164.0/255.0) green:(206.0/255.0) blue:(60.0/255.0) alpha:1.0].CGColor, (id)[UIColor colorWithRed:(16.0/255.0) green:(112.0/255.0) blue:(49.0/255.0) alpha:1.0].CGColor];
	[saveButton.layer insertSublayer:saveButtonGradient atIndex:0];
	[saveButton setTitle:NSLocalizedString(@"Save", @"Save") forState:UIControlStateNormal];
	[saveButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
	[saveButton addTarget:self action:@selector(buttonSaveAction:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:saveButton];
	
	deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.origin.x + self.frame.size.width * 0.5, self.frame.origin.y + 2, self.frame.size.width * 0.5 - 2, self.frame.size.height - 4)];
	deleteButton.layer.cornerRadius = 3.0;
	CAGradientLayer *deleteButtonGradient = [CAGradientLayer layer];
	deleteButtonGradient.frame = deleteButton.bounds;
	deleteButtonGradient.colors = @[(id)[UIColor colorWithRed:(240.0/255.0) green:(55.0/255.0) blue:(55.0/255.0) alpha:1.0].CGColor, (id)[UIColor colorWithRed:(140.0/255.0) green:(24.0/255.0) blue:(24.0/255.0) alpha:1.0].CGColor];
	[deleteButton.layer insertSublayer:deleteButtonGradient atIndex:0];
	[deleteButton setTitle:NSLocalizedString(@"Delete", @"Delete") forState:UIControlStateNormal];
	[deleteButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
	[deleteButton addTarget:self action:@selector(buttonDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:deleteButton];
}

- (void)removeSaveDeleteButtons {
	[saveButton removeFromSuperview];
	saveButton = nil;
	[deleteButton removeFromSuperview];
	deleteButton = nil;
	[self.layer setNeedsDisplay];
	[self.layer displayIfNeeded];
}

@end
