//
//  PCSliderView.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 20.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IPCMainViewProtocol;

@interface PCSliderView : UIView <UIGestureRecognizerDelegate>

@property (nonatomic) float delayForStart;
@property (nonatomic, strong) id<IPCMainViewProtocol> parentVC;

@end
