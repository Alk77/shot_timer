//
//  PCShootingResults.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 21.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import "PCShootingResults.h"

@implementation PCShootingResults

- (instancetype)init {
	@throw [NSException exceptionWithName:@"PCShootingResults"
								   reason:@"Use [[PCShootingResults alloc] initWithOverallTime:(float)overallTime firstShotTime:(float)fistShotTime shotsAmount:(int)shotsAmount timePerShot:(float)timePerShot]"
								 userInfo:nil];
	return nil;
}

- (instancetype)initWithOverallTime:(float)overallTime firstShotTime:(float)fistShotTime shotsAmount:(int)shotsAmount timePerShot:(float)timePerShot {
	self = [super init];
	if (self) {
		self.overallTime = [NSNumber numberWithFloat:overallTime];
		self.firstShotTime = [NSNumber numberWithFloat:fistShotTime];
		self.shotsAmount = [NSNumber numberWithInt:shotsAmount];
		self.timePerShot = [NSNumber numberWithFloat:timePerShot];
		self.shootingDate = [NSDate date];
	}
	return self;
}

#pragma mark - NSCoding delegates

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super init];
	if (self)
	{
		self.overallTime = [aDecoder decodeObjectForKey:@"overallTime"];
		self.firstShotTime = [aDecoder decodeObjectForKey:@"firstShotTime"];
		self.shotsAmount = [aDecoder decodeObjectForKey:@"shotsAmount"];
		self.timePerShot = [aDecoder decodeObjectForKey:@"timePerShot"];
		self.shootingDate = [aDecoder decodeObjectForKey:@"shootingDate"];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
	[aCoder encodeObject:self.overallTime forKey:@"overallTime"];
	[aCoder encodeObject:self.firstShotTime forKey:@"firstShotTime"];
	[aCoder encodeObject:self.shotsAmount forKey:@"shotsAmount"];
	[aCoder encodeObject:self.timePerShot forKey:@"timePerShot"];
	[aCoder encodeObject:self.shootingDate forKey:@"shootingDate"];
}

@end
