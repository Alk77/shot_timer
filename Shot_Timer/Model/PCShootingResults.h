//
//  PCShootingResults.h
//  Shot_Timer
//
//  Created by ProCreationsMac on 21.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCShootingResults : NSObject

@property (nonatomic, strong) NSNumber *overallTime;
@property (nonatomic, strong) NSNumber *firstShotTime;
@property (nonatomic, strong) NSNumber *shotsAmount;
@property (nonatomic, strong) NSNumber *timePerShot;
@property (nonatomic, strong) NSDate *shootingDate;

- (instancetype)initWithOverallTime:(float)overallTime firstShotTime:(float)fistShotTime shotsAmount:(int)shotsAmount timePerShot:(float)timePerShot;

@end
