//
//  PCConstants.m
//  Shot_Timer
//
//  Created by ProCreationsMac on 21.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import "PCConstants.h"

@implementation PCConstants

#pragma mark - user defaults keys

NSString * const shotResultsKey = @"shotResults";
NSString * const delayTimeKey = @"delayTime";
NSString * const sensitivityKey = @"sensitivity";
NSString * const isRandomTimeKey = @"isRandomTime";

@end
