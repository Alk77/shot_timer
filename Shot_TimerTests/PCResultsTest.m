//
//  PCResultsTest.m
//  Shot_TimerTests
//
//  Created by ProCreationsMac on 02.02.2018.
//  Copyright © 2018 ProCreationsMac. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCHamcrest/OCHamcrest.h>
#import <OCMockito/OCMockito.h>
#import "PCResultsPresenter.h"
#import "IPCResultsProtocol.h"

@interface PCResultsTest : XCTestCase

@property (nonatomic, strong) PCResultsPresenter *presenter;
@property (nonatomic, strong) id<IPCResultsProtocol> view;

@end

@implementation PCResultsTest

- (void)setUp {
    [super setUp];
	self.presenter = [[PCResultsPresenter alloc] init];
	self.view = mockProtocol(@protocol(IPCResultsProtocol));
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSaveResults {
	[self.presenter updateResultsInView:self.view];
	[verify(self.view) updateResultsTableView];
}

- (void)testDeleteResult {
	[self.presenter deleteResult:0 inView:self.view];
	[verify(self.view) updateResultsTableView];
}

@end
