//
//  PCMainViewTests.m
//  Shot_TimerTests
//
//  Created by ProCreationsMac on 20.12.2017.
//  Copyright © 2017 ProCreationsMac. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCHamcrest/OCHamcrest.h>
#import <OCMockito/OCMockito.h>
#import "NSError+PCError.h"
#import "PCMainViewController.h"
#import "PCMainPresenter.h"
#import "IPCMainViewProtocol.h"

@interface PCMainViewTests : XCTestCase

@property (nonatomic, strong) PCMainPresenter *presenter;
@property (nonatomic, strong) id<IPCMainViewProtocol> view;

@end

@implementation PCMainViewTests

- (void)setUp {
    [super setUp];
	self.presenter = [[PCMainPresenter alloc] init];
	self.view = mockProtocol(@protocol(IPCMainViewProtocol));
}

- (void)tearDown {
    [super tearDown];
}

- (void)testStartTimer {
	[self.presenter startTimerInView:self.view];
#if TARGET_OS_SIMULATOR
	[verify(self.view) startTimerUpdateViewWithError:[NSError getErrorWithErrorCode:PCCannotLaunchOnSimulaton]];
#elif
	[verify(self.view) startTimerUpdateViewWithError:nil];
#endif
}

- (void)testStopTimer {
	[self.presenter stopTimerInView:self.view];
	[verify(self.view) stopTimerUpdateViewWithOverallTime:0.0 firstShotTime:0.0 shotsAmount:0 timePerShot:0.0];
}

- (void)testSaveResults {
	[self.presenter saveResultsInView:self.view];
	[verify(self.view) saveResultsUpdateView];
}

@end
